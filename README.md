# OSU LUG Infrastructure

See our documentation repo's [contributing.md](https://gitlab.com/osu-lug/projects/documentation/-/blob/master/docs/contributing.md) file for current documentation on CI / CD (Git policy for Merge Requests, maintainer instructions for deploying documentation website updates, etc).

In the future, we might move some infrastructure documentation to this README as well.
