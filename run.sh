#!/bin/bash
docker images | grep -v '<none>' | grep -v 'local' | tail -n +2 | awk '{print $1}' | while read line; do docker pull $line;done
cd /home/debian/data/mkdocs/docs
sudo git pull
sudo cp ../mkdocs.yml ../mkdocs.yml.bak
sudo cat ../mkdocs.yml.bak | grep 'nav:' -B 100000 | sudo tee ../mkdocs.yml
sudo bash -c 'python3 /home/debian/mkdocs/generate-navigation.py >> /home/debian/data/mkdocs/docs/../mkdocs.yml;echo "# DO NOT PUT ANYTHING BELOW THE NAV TAG NOR EDIT DIRECTLY" >> /home/debian/data/mkdocs/docs/../mkdocs.yml'
sudo rm ../mkdocs.yml.bak
cd /home/debian/data/hugo/src
sudo git pull

cd
docker-compose -f docker-compose-build.yml up --force-recreate --build
#docker-compose -f docker-compose-run.yml down
docker-compose -f docker-compose-run.yml up -d --build
