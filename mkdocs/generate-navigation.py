from os import walk
data ={}
output=[]
for item in walk("."):
    if item[0].startswith("./.git") or item[0].startswith("./stylesheet"):
        continue
    for splited in item[0].split('/'):
        if splited =='.':
            continue
    data[item[0][2:]]=item[1]+item[2]
## Custom movement
data['']=['Home','Install','Linux','Applications','Tools','FOSS','History','OSU','Meetings']
data['Home']=[['Home','../index.md'],'../resources.md','../LUG.md','../contributing.md',["Home Page","https://lug.oregonstate.edu"]]
data['Install']= ['general.md', 'bootable-usb.md', 'Target','installation.md', 'post-install.md', 'notes.md']
data['Install/Target']= [['Physical','bare-metal.md'],'VirtualBox.md','WSL.md']
data['Linux']= ['basic-commands.md','distros.md','Components', 'Security', 'Other-NIX', 'Compartmentalization', 'programming-langs.md', 'backups.md', 'rescue.md', 'reading-manpages.md', 'users.md']
data['Linux/Components']= ['desktop-environment.md','folder-layout.md','partitions.md','driver.md', 'networking.md', 'bios.md', 'package-manager.md', 'display-server.md', 'permissions.md', 'boot-loader.md', 'file-input.md', 'tty.md', 'login-manager.md', 'kernel.md', 'kernel-module.md', 'window-manager.md']
data['Applications']= ['general.md','Text-Editor', 'Browsers', 'Services', 'Editors', 'Fun']


def print_data(data):
    for item in data:
        print("data['"+item+"']=",data[item])
    exit()
#print_data(data)
## END custom movement
def print_tree(data,key,key_display,tab):
    if key_display!='':
        for i in range(0,tab):
            print(' ',end='')
        print('- '+key_display+':')
    for node in data[key]:
        if (not isinstance(node,list) and (node.endswith(".md") or node.startswith("https://"))) or (isinstance(node,list) and (node[1].endswith(".md") or node[1].startswith("https://"))):
            # File
            output_item=''
            if key !='' and not ((not isinstance(node,list) and node.startswith("https://") or (isinstance(node,list) and node[1].startswith("https://")))):
                output_item+=key+'/'
            if isinstance(node,list):
                output_item=node[0]+': '+output_item+node[1]
            else:
                output_item+=node
            for i in range(0,tab+4):
                print(' ',end='')
            print('- '+output_item)
        elif not isinstance(node,list):
            # Directory
            if key+'/'+node in data:
                print_tree(data,key+'/'+node,node,tab+4)
            elif key=='' and node in data:
                print_tree(data,node,node,tab)
print_tree(data,'','',4)
